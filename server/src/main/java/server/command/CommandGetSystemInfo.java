package server.command;

import command.CommandException;
import utils.SystemUtils;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;

public class CommandGetSystemInfo extends CommandToFrontend {
    private final UserProfile user;

    public CommandGetSystemInfo(Address from, UserProfile user) throws ServiceException {
        super(from);
        this.user = user;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        Message message = new SystemMessage(SystemUtils.instance.GetInfo());
        sendCommand(new CommandSendOneMessage(getTo(), user, message));
    }

    @Override
    public String toString() {
        return "CommandGetSystemInfo{user=" + user + '}';
    }
}

package server.command;

import command.CommandException;
import server.account.UserProfile;
import server.message.MessageService;
import service.Address;
import service.ServiceException;

public class CommandGetLastMessages extends CommandToMessage {
    private final UserProfile user;

    public CommandGetLastMessages(Address from, UserProfile user) throws ServiceException {
        super(from);
        this.user = user;
    }

    public UserProfile getUser() {
        return user;
    }

    @Override
    public void exec(MessageService messageService) throws ServiceException, CommandException {
        sendCommand(new CommandSendManyMessages(getTo(), user, messageService.getLastMessages(100)));
    }

    @Override
    public String toString() {
        return "CommandGetLastMessages{" + "user=" + user.toString() + '}';
    }
}

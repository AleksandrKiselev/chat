package server.command;

import command.CommandException;
import command.CommandRegistry;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;

public class CommandGetHelp extends CommandToFrontend {
    private final UserProfile user;

    public CommandGetHelp(Address from, UserProfile user) throws ServiceException {
        super(from);
        this.user = user;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        Message message = new SystemMessage(CommandRegistry.instance.getHelp());
        sendCommand(new CommandSendOneMessage(getTo(), user, message));
    }

    @Override
    public String toString() {
        return "CommandGetHelp{user=" + user.toString() + '}';
    }
}

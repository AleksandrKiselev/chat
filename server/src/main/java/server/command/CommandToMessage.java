package server.command;

import command.CommandException;
import command.CommandTo;
import server.message.MessageService;
import service.Address;
import service.IService;
import service.ServiceException;

public abstract class CommandToMessage extends CommandTo {
    public CommandToMessage(Address from) throws ServiceException {
        super(from, MessageService.MESSAGE_SERVICE_TYPE);
    }

    @Override
    public void exec(IService service) throws ServiceException, CommandException {
        validateServiceType(service);
        exec((MessageService)service);
    }

    public abstract void exec(MessageService messageService) throws ServiceException, CommandException;
}

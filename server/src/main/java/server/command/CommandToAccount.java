package server.command;

import command.CommandException;
import command.CommandTo;
import server.account.AccountService;
import service.Address;
import service.IService;
import service.ServiceException;

public abstract class CommandToAccount extends CommandTo {
    public CommandToAccount(Address from) throws ServiceException {
        super(from, AccountService.ACCOUNT_SERVICE_TYPE);
    }

    @Override
    public void exec(IService service) throws ServiceException, CommandException {
        validateServiceType(service);
        exec((AccountService) service);
    }

    public abstract void exec(AccountService accountService) throws ServiceException, CommandException;
}

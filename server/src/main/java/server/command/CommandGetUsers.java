package server.command;

import command.CommandException;
import server.account.AccountService;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.message.Message;
import service.Address;
import service.ServiceException;

public class CommandGetUsers extends CommandToAccount {
    private final UserProfile user;

    public CommandGetUsers(Address from, UserProfile user) throws ServiceException {
        super(from);
        this.user = user;
    }

    @Override
    public void exec(AccountService accountService) throws ServiceException, CommandException {
        Message message = new SystemMessage(accountService.getUsersString());
        sendCommand(new CommandSendOneMessage(getTo(), user, message));
    }

    @Override
    public String toString() {
        return "CommandGetUsers{user=" + user.toString() + '}';
    }
}

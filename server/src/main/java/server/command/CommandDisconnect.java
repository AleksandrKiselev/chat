package server.command;

import command.CommandException;
import network.ConnectionId;
import server.frontend.FrontendService;
import service.Address;
import service.ServiceException;

public class CommandDisconnect extends CommandToFrontend {
    private final ConnectionId id;

    public CommandDisconnect(Address from, ConnectionId id) throws ServiceException {
        super(from);
        this.id = id;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        frontendService.disconnect(id);
    }

    @Override
    public String toString() {
        return "CommandDisconnect{id=" + id.toString() + '}';
    }
}

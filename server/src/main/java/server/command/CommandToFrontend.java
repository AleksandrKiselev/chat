package server.command;

import command.CommandException;
import command.CommandTo;
import server.frontend.FrontendService;
import service.Address;
import service.IService;
import service.ServiceException;

public abstract class CommandToFrontend extends CommandTo {
    public CommandToFrontend(Address from) throws ServiceException {
        super(from, FrontendService.FRONTEND_SERVICE_TYPE);
    }

    @Override
    public void exec(IService service) throws ServiceException, CommandException {
        validateServiceType(service);
        exec((FrontendService)service);
    }

    public abstract void exec(FrontendService frontendService) throws ServiceException, CommandException;
}

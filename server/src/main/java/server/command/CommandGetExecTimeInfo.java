package server.command;

import command.CommandException;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;
import utils.CommandUtils;

public class CommandGetExecTimeInfo extends CommandToFrontend {
    private final UserProfile user;

    public CommandGetExecTimeInfo(Address from, UserProfile user) throws ServiceException {
        super(from);
        this.user = user;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        Message message = new SystemMessage(CommandUtils.instance.getExecTimeInfo());
        sendCommand(new CommandSendOneMessage(getTo(), user, message));
    }

    @Override
    public String toString() {
        return "CommandGetExecTimeInfo{user=" + user + '}';
    }
}

package server.command;

import command.CommandException;
import network.ConnectionId;
import server.account.AccountService;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.frontend.FrontendService;
import service.Address;
import service.ServiceException;

public class CommandLogin extends CommandToAccount {
    private final static String WELCOME_MESSAGE = "%s, Welcome! Commands list - !help\n";
    private final static String ERR_NAME_IS_EXIST = "Username already in use";
    private final static String ERR_NAME_IS_INVALID = "Incorrect username. " +
            "Allowed only letters, numbers and '_'. (3-30 characters)";

    private final ConnectionId id;
    private final String name;

    public CommandLogin(Address from, ConnectionId id, String name) throws ServiceException {
        super(from);
        this.id = id;
        this.name = name;
    }

    @Override
    public void exec(AccountService accountService) throws ServiceException, CommandException {
        if (!accountService.isValidUser(name)) {
            sendCommand(new CommandLoginFailure(getTo(), ERR_NAME_IS_INVALID));
            return;
        }
        if (accountService.isExistUser(name)) {
            sendCommand(new CommandLoginFailure(getTo(), ERR_NAME_IS_EXIST));
            return;
        }

        UserProfile user = accountService.addUser(name);
        sendCommand(new CommandLoginSuccess(getTo(), user));
    }

    @Override
    public String toString() {
        return "CommandLogin{id=" + id + ", name='" + name + '\'' + '}';
    }


    /*------------------------------------------------------------------------------------------------------------
    Response to server.frontend commands (success, failure)
    ------------------------------------------------------------------------------------------------------------*/
    private class CommandLoginSuccess extends CommandToFrontend {
        private final UserProfile user;

        public CommandLoginSuccess(Address from, UserProfile user) throws ServiceException {
            super(from);
            this.user = user;
        }

        @Override
        public void exec(FrontendService frontendService) throws ServiceException, CommandException {
            frontendService.addSession(user, id);
            frontendService.send(user, new SystemMessage(String.format(WELCOME_MESSAGE, user.getUsername())));
        }

        @Override
        public String toString() {
            return "CommandLoginSuccess{user=" + user + '}';
        }
    }

    private class CommandLoginFailure extends CommandToFrontend {
        private final String error;

        public CommandLoginFailure(Address from, String error) throws ServiceException  {
            super(from);
            this.error = error;
        }

        @Override
        public void exec(FrontendService frontendService) throws ServiceException, CommandException {
            frontendService.send(id, new SystemMessage(error));
        }

        @Override
        public String toString() {
            return "CommandLoginFailure{error='" + error + '\'' + '}';
        }
    }
}

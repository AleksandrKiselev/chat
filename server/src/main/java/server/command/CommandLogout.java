package server.command;

import command.CommandException;
import server.account.AccountService;
import server.account.UserProfile;
import server.frontend.FrontendService;
import service.Address;
import service.ServiceException;

public class CommandLogout extends CommandToAccount {
    private final static String GOODBYE_MESSAGE = "%s, Goodbye!\n";
    private final UserProfile user;

    public CommandLogout(Address from, UserProfile user) throws ServiceException  {
        super(from);
        this.user = user;
    }

    @Override
    public void exec(AccountService accountService) throws ServiceException, CommandException {
        if (accountService.isExistUser(user.getUsername())) {
            accountService.removeUser(user);
            sendCommand(new CommandLogoutResponse(getTo()));
        }
    }

    @Override
    public String toString() {
        return "CommandLogout{user=" + user + '}';
    }


    /*------------------------------------------------------------------------------------------------------------
    Response to server.frontend commands
    ------------------------------------------------------------------------------------------------------------*/
    private class CommandLogoutResponse extends CommandToFrontend {
        public CommandLogoutResponse(Address from) throws ServiceException {
            super(from);
        }

        @Override
        public void exec(FrontendService frontendService) throws ServiceException, CommandException  {
            frontendService.send(user, String.format(GOODBYE_MESSAGE, user.getUsername()));
            frontendService.removeSession(user);
        }

        @Override
        public String toString() {
            return "CommandLogoutResponse{user=" + user.toString() + '}';
        }
    }
}

package server.command;

import command.CommandException;
import server.account.UserProfile;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;

public class CommandSendOneMessage extends CommandToFrontend {
    private final UserProfile user;
    private final Message message;

    public CommandSendOneMessage(Address from, UserProfile user, Message message) throws ServiceException {
        super(from);
        this.user = user;
        this.message = message;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        frontendService.send(user, message);
    }

    @Override
    public String toString() {
        return "CommandSendOneMessage{user=" + user + ", message=" + message + '}';
    }
}

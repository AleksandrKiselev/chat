package server.command;

import command.CommandException;
import server.account.UserProfile;
import server.message.Message;
import server.message.MessageService;
import service.Address;
import service.ServiceException;

import java.util.List;

public class CommandHandleMessages extends CommandToMessage {
    private final UserProfile user;
    private final List<String> messages;

    public CommandHandleMessages(Address from, UserProfile user, List<String> messages) throws ServiceException {
        super(from);
        this.user = user;
        this.messages = messages;
    }

    public UserProfile getUser() {
        return user;
    }

    public List<String> getMessages() {
        return messages;
    }

    @Override
    public void exec(MessageService messageService) throws ServiceException, CommandException {
        String messageText = "";
        for (String m: getMessages()) {
            Message message = messageService.parse(user, m);
            if (message != null) {
                messageText += message.getText() + "\n";
            }
        }
        if (!messageText.isEmpty()) {
            sendCommand(new CommandBroadcast(getTo(), new Message(user, messageText)));
        }
    }

    @Override
    public String toString() {
        return "CommandHandleMessages{user=" + user.toString() + ", messages=" + getMessages() + '}';
    }
}

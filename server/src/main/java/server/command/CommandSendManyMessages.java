package server.command;

import command.CommandException;
import server.account.UserProfile;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;

import java.util.List;

public class CommandSendManyMessages extends CommandToFrontend {
    private final List<Message> messages;
    private final UserProfile user;

    public CommandSendManyMessages(Address from, UserProfile user, List<Message> messages) throws ServiceException {
        super(from);
        this.messages = messages;
        this.user = user;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        frontendService.send(user, messages);
    }

    @Override
    public String toString() {
        return "CommandSendManyMessages{" + "user=" + user + ", messages=" + messages + '}';
    }
}

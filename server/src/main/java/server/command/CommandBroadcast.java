package server.command;

import command.CommandException;
import server.frontend.FrontendService;
import server.message.Message;
import service.Address;
import service.ServiceException;

public class CommandBroadcast extends CommandToFrontend {
    private final Message message;

    public CommandBroadcast(Address from, Message message) throws ServiceException {
        super(from);
        this.message = message;
    }

    @Override
    public void exec(FrontendService frontendService) throws ServiceException, CommandException {
        frontendService.broadcast(message);
    }

    @Override
    public String toString() {
        return "CommandBroadcast{message=" + message.toString() + '}';
    }
}

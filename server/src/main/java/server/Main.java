package server;

import command.CommandRegistry;
import network.TCPSocketServerConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.account.AccountService;
import server.command.*;
import server.frontend.FrontendService;
import server.message.MessageService;
import server.utils.ServerUtils;
import service.ServiceRegistry;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());
    private static final ServiceRegistry SERVREG = ServiceRegistry.instance;

    private static void registryCommands() {
        CommandRegistry commandRegistry = CommandRegistry.instance;
        commandRegistry.registry("!help", CommandGetHelp.class, "all commands");
        commandRegistry.registry("!info", CommandGetSystemInfo.class, "system information");
        commandRegistry.registry("!exectime", CommandGetExecTimeInfo.class, "command execution time information");
        commandRegistry.registry("!users", CommandGetUsers.class, "all users");
        commandRegistry.registry("!last", CommandGetLastMessages.class, "last 100 messages");
        commandRegistry.registry("!logout", CommandLogout.class, "logout");
    }

    public static void main(String[] args) {
        LOGGER.info("Server started.\n");

        registryCommands();
        try {
            SERVREG.register(new AccountService(1));
            SERVREG.register(new MessageService(1));
            SERVREG.register(new FrontendService(
                    new TCPSocketServerConnection(ServerUtils.instance.getPort()))
            );
            SERVREG.startAll();
        }

        catch (Exception e) {
            SERVREG.stopAll();
            LOGGER.error(e);
        }
    }
}

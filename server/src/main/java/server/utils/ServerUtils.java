package server.utils;

import utils.Utils;

public class ServerUtils extends Utils {
    public static final ServerUtils instance = new ServerUtils();
    private ServerUtils() { super("cfg/server.properties", ""); }

    private int port;
    private int frontendWorkersCount;
    private boolean frontendAsyncCommand;

    @Override
    protected void InitializeSettings() {
        port = Integer.parseInt(getProperty("port", "8083"));
        frontendWorkersCount = Integer.parseInt(getProperty("frontendWorkersCount", "100"));
        frontendAsyncCommand = Boolean.parseBoolean(getProperty("frontendAsyncCommand", "true"));
    }

    @Override
    public String toString() {
        return "port=" + port +
                ", frontendWorkersCount=" + frontendWorkersCount +
                ", frontendAsyncCommand=" + frontendAsyncCommand;
    }

    public int getPort() {
        return port;
    }

    public int getFrontendWorkersCount() {
        return frontendWorkersCount;
    }

    public boolean isFrontendAsyncCommand() {
        return frontendAsyncCommand;
    }
}

package server.message;

import server.account.UserProfile;
import command.CommandQueue;
import command.CommandRegistry;
import command.ICommand;
import service.IService;

public class Message {
    public final static char COMMAND_MESSAGE_PREFIX = '!';
    public final static String MESSAGE_FORMAT = "[%1$s] %2$s";
    private final UserProfile user;
    private final String text;

    public Message(UserProfile user, String text) {
        this.user = user;
        this.text = text;
    }

    public UserProfile getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        String result = getText();
        if (user != null)
            result = String.format(MESSAGE_FORMAT, getUser().toString(), getText());
        if (!result.endsWith("\n"))
            result += "\n";
        return result;
    }

    public static Message parse(IService service, UserProfile user, String text) {
        if (text.length() == 0) return null;
        if (text.charAt(0) == COMMAND_MESSAGE_PREFIX) {
            String[] strings = text.split(" ");

            ICommand command = CommandRegistry.instance.getCommand(strings[0], service.getAddress(), user);
            if (command != null) {
                CommandQueue.instance.send(command);
                return null;
            }
        }
        return new Message(user, text);
    }
}

package server.message;

import server.account.UserProfile;
import service.CommandService;
import service.ServiceType;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class MessageService extends CommandService {
    public final static ServiceType MESSAGE_SERVICE_TYPE = new ServiceType("MessageService");
    private ConcurrentLinkedDeque<Message> messages = new ConcurrentLinkedDeque <>();

    public MessageService(int workersCount) {
        super(MESSAGE_SERVICE_TYPE, workersCount, false);
    }

    public Message parse(UserProfile user, String text) {
        Message message = Message.parse(this, user, text);
        if (message != null) {
            messages.add(message);
        }
        return message;
    }

    public List<Message> getLastMessages(int count) {
        LinkedList<Message> result = new LinkedList<>();

        Iterator<Message> iterator = messages.descendingIterator();
        while (iterator.hasNext() && count > 0) {
            result.add(iterator.next());
            count--;
        }
        Collections.reverse(result);
        return result;
    }
}

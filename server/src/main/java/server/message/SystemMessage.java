package server.message;

import server.account.AccountService;

public class SystemMessage extends Message {
    public SystemMessage(String text) {
        super(AccountService.SYSTEM_USER, text);
    }
}

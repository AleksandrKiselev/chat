package server.account;

import service.CommandService;
import service.ServiceType;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountService extends CommandService {
    public final static Pattern USERNAME_PATTERN = Pattern.compile("^[А-ЯЁа-яёA-Za-z0-9_]{3,30}$");
    public final static ServiceType ACCOUNT_SERVICE_TYPE = new ServiceType("AccountService");
    public final static UserProfile SYSTEM_USER = new UserProfile("SystemUser");

    private final ConcurrentLinkedQueue<UserProfile> users = new ConcurrentLinkedQueue<>();

    public AccountService(int workersCount) {
        super(ACCOUNT_SERVICE_TYPE, workersCount, false);
    }

    public boolean isValidUser(String name) {
        Matcher matcher = USERNAME_PATTERN.matcher(name);
        return matcher.matches();
    }

    public boolean isExistUser(String name) {
        for (UserProfile u: users) {
            if (name.compareTo(u.getUsername()) == 0) {
                return true;
            }
        }
        return false;
    }

    public String getUsersString() {
        String result = "Users: ";
        for (UserProfile u: users) {
            result += u.getUsername() + " ";
        }
        return result;
    }

    public UserProfile addUser(String name) {
        UserProfile user = new UserProfile(name);
        users.add(user);
        getLogger().info("add user " + name);
        return user;
    }

    public boolean removeUser(UserProfile user) {
        for (UserProfile u: users) {
            if (u == user) {
                getLogger().info("remove user " + u.getUsername());
                return users.remove(u);
            }
        }
        return false;
    }
}

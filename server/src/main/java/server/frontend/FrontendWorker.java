package server.frontend;

import service.ServiceException;
import service.worker.ServiceWorker;

public class FrontendWorker extends ServiceWorker {
    public FrontendWorker(FrontendService frontendService) {
        super(frontendService.getType().toString() + ":FrontendWorker", frontendService);
    }

    protected FrontendService getFrontendService() {
        return (FrontendService)getService();
    }

    @Override
    protected boolean doWork() throws InterruptedException, ServiceException {
        return getFrontendService().getConnectionHandler().handleConnectionActivity(true);
    }

    @Override
    protected void free() {
        getService().stop();
    }
}

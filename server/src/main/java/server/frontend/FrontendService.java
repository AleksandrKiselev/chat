package server.frontend;

import network.ConnectionId;
import network.IServerConnectionHandler;
import network.TCPSocketConnection;
import network.TCPSocketServerConnection;
import server.message.SystemMessage;
import server.account.UserProfile;
import server.command.*;
import server.message.Message;
import server.utils.ServerUtils;
import service.CommandService;
import service.ServiceException;
import service.ServiceType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FrontendService extends CommandService {
    public final static ServiceType FRONTEND_SERVICE_TYPE = new ServiceType("FrontendService");

    private final ConcurrentHashMap<UserProfile, ConnectionId> sessions = new ConcurrentHashMap<>();
    private IServerConnectionHandler connectionHandler;

    public FrontendService(IServerConnectionHandler connectionHandler) {
        super(
                FRONTEND_SERVICE_TYPE,
                ServerUtils.instance.getFrontendWorkersCount(),
                ServerUtils.instance.isFrontendAsyncCommand()
        );
        this.connectionHandler = connectionHandler;

        getConnectionHandler().setOnAcceptCallback(this::onAcceptCallback);
        getConnectionHandler().setOnReadCallback(this::onReadCallback);
        getConnectionHandler().setOnCloseCallback(this::onCloseCallback);
        getConnectionHandler().setOnThrowCallback(this::onThrowCallback);

        addWorker(new FrontendWorker(this), false);
    }

    private boolean onAcceptCallback(ConnectionId id){
        getLogger().info("accepted connection from " + id);
        return true;
    }

    private boolean onReadCallback(ConnectionId id, String text) {
        ArrayList<String> messages = new ArrayList<>(Arrays.asList(text.split("\n")));
        messages.removeIf(m -> "".equals(m));
        if (messages.size() == 0) return false;

        try {
            UserProfile user = findSession(id);
            if (user == null) {
                sendCommand(new CommandLogin(getAddress(), id, messages.get(0)));
                messages.remove(0);
            } else {
                sendCommand(new CommandHandleMessages(getAddress(), user, messages));
            }
        } catch (ServiceException e) { return false; }
        return true;
    }

    private boolean onCloseCallback(ConnectionId id, boolean closed) {
        getLogger().info("connection  " + id + " closed.");
        return disconnect(id);
    }

    private boolean onThrowCallback(Exception e) {
        getLogger().error(e);
        return true;
    }


    // public service methods

    public IServerConnectionHandler getConnectionHandler() {
        if (connectionHandler == null)
            connectionHandler = new TCPSocketServerConnection(TCPSocketConnection.DEFAULT_PORT);
        return connectionHandler;
    }

    public boolean disconnect(ConnectionId id) {
        if (id == null) return false;
        UserProfile user = findSession(id);
        if (user != null) {
            removeSession(user);
            try {
                sendCommand(new CommandLogout(getAddress(), user));
                sendCommand(new CommandBroadcast(
                        getAddress(),
                        new SystemMessage(user.getUsername() + " was disconnected.")
                ));
            }
            catch (ServiceException e) { getLogger().error(e); }
        }
        return getConnectionHandler().close(id);
    }

    public ConcurrentHashMap<UserProfile, ConnectionId> getSessions() {
        return sessions;
    }

    public void addSession(UserProfile user, ConnectionId id) {
        broadcast(new SystemMessage(user.getUsername() + " was connected."));
        getSessions().put(user, id);

        try { sendCommand(new CommandGetLastMessages(getAddress(), user)); }
        catch (ServiceException e) { getLogger().error(e); }
    }

    public void removeSession(UserProfile user) {
        getSessions().remove(user);
    }

    public UserProfile findSession(ConnectionId id) {
        for (Map.Entry<UserProfile, ConnectionId> e: getSessions().entrySet()) {
            if (e.getValue() == id) {
                return e.getKey();
            }
        }
        return null;
    }

    public void broadcast(Message message) {
        broadcast(message.getUser(), message.toString());
    }

    public void broadcast(UserProfile user, String text) {
        if (getSessions().isEmpty()) return;

        ArrayList<ConnectionId> ids = new ArrayList<>();
        for (Map.Entry<UserProfile, ConnectionId> e: getSessions().entrySet()) {
            if (e.getKey() == user) continue;
            ids.add(e.getValue());
        }

        getConnectionHandler().broadcast(ids, text);
    }

    public void send(ConnectionId id, String text) {
        if (!getConnectionHandler().send(id, text)) {
            try { sendCommand(new CommandDisconnect(getAddress(), id)); }
            catch (ServiceException e) { getLogger().error(e); }
        }
    }

    public void send(ConnectionId id, Message message) {
        send(id, message.toString());
    }

    public void send(ConnectionId id, List<Message> messages) {
        if (messages.isEmpty()) return;
        String text = "";
        for (Message m: messages) {
            text += m.toString();
        }
        send(id, text);
    }

    public void send(UserProfile user, String text) {
        if (!getSessions().containsKey(user)) return;
        send(getSessions().get(user), text);
    }

    public void send(UserProfile user, Message message) {
        if (!getSessions().containsKey(user)) return;
        send(getSessions().get(user), message);
    }

    public void send(UserProfile user, List<Message> messages) {
        if (!getSessions().containsKey(user)) return;
        send(getSessions().get(user), messages);
    }
}

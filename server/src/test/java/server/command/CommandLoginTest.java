package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import static org.mockito.Mockito.*;

public class CommandLoginTest  extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        doReturn(user).when(accountServiceMock).addUser(username);
        doReturn(false).when(accountServiceMock).isExistUser(username);
        doReturn(true).when(accountServiceMock).isValidUser(username);

        ICommand command = new CommandLogin(address1, connectionId, username);
        command.exec(accountServiceMock);
        verify(accountServiceMock).isValidUser(username);
        verify(accountServiceMock).isExistUser(username);
        verify(accountServiceMock).addUser(username);
    }
}
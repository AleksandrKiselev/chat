package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;
import server.message.Message;

import java.util.LinkedList;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommandSendManyMessagesTest  extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        LinkedList<Message> messages = new LinkedList<>();
        messages.add(message);
        messages.add(message);
        messages.add(message);

        ICommand command = new CommandSendManyMessages(address1, user, messages);
        command.exec(frontendServiceMock);
        verify(frontendServiceMock, times(1)).send(user, messages);
    }
}
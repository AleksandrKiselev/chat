package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import static org.mockito.Mockito.verify;

public class CommandDisconnectTest extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        ICommand command = new CommandDisconnect(address1, connectionId);
        command.exec(frontendServiceMock);
        verify(frontendServiceMock).disconnect(connectionId);
    }
}
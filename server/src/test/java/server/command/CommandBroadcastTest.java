package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;
import static org.mockito.Mockito.*;

public class CommandBroadcastTest extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        ICommand command = new CommandBroadcast(address1, message);
        command.exec(frontendServiceMock);
        verify(frontendServiceMock).broadcast(message);
    }
}
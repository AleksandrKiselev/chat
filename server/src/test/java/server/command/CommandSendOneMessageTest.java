package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommandSendOneMessageTest  extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        ICommand command = new CommandSendOneMessage(address1, user, message);
        command.exec(frontendServiceMock);
        verify(frontendServiceMock, times(1)).send(user, message);
    }
}
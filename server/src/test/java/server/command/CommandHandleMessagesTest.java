package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import java.util.LinkedList;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommandHandleMessagesTest  extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        LinkedList<String> messages = new LinkedList<>();
        messages.add(messageText);
        messages.add(messageText);
        messages.add(messageText);

        ICommand command = new CommandHandleMessages(address1, user, messages);
        command.exec(messageServiceMock);
        verify(messageServiceMock, times(3)).parse(user, messageText);
    }
}
package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CommandLogoutTest  extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        doReturn(true).when(accountServiceMock).isExistUser(username);

        ICommand command = new CommandLogout(address1, user);
        command.exec(accountServiceMock);
        verify(accountServiceMock).removeUser(user);
    }
}
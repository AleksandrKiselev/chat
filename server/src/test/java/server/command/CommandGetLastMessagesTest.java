package server.command;

import command.ICommand;
import org.junit.Test;
import server.BaseServiceTest;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

public class CommandGetLastMessagesTest extends BaseServiceTest {
    @Test
    public void exec() throws Exception {
        ICommand command = new CommandGetLastMessages(address1, user);
        command.exec(messageServiceMock);
        verify(messageServiceMock).getLastMessages(anyInt());
    }
}
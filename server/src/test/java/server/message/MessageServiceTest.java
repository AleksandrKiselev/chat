package server.message;

import org.junit.Assert;
import org.junit.Test;
import server.account.UserProfile;

import java.util.List;

public class MessageServiceTest {
    private MessageService messageService = new MessageService(1);
    private UserProfile user = new UserProfile("tester");

    @Test
    public void parse() throws Exception {
        String text = "message text";
        Message message = messageService.parse(user, text);
        Assert.assertNotNull(message);
        Assert.assertEquals(text, message.getText());
        Assert.assertEquals(user, message.getUser());
    }

    @Test
    public void getLastMessages() throws Exception {
        int count = 20;
        for (int i = 0; i < count; i++) {
            messageService.parse(user, Integer.toString(i));
        }

        List<Message> last = messageService.getLastMessages(2);
        Assert.assertTrue(last.size() == 2);
        Assert.assertEquals(new Message(user, "18").toString(), last.get(0).toString());
        Assert.assertEquals(new Message(user, "19").toString(), last.get(1).toString());
    }

}
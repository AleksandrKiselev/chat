package server.frontend;

import command.ICommand;
import network.ConnectionId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import server.BaseServiceTest;
import server.account.UserProfile;

import java.util.Map;

import static org.mockito.Mockito.*;


public class FrontendServiceTest extends BaseServiceTest {
    private FrontendService frontendService;
    private ServerConnHandlerStub connHandlerStub;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        connHandlerStub = spy(new ServerConnHandlerStub(10, messageText));
        frontendService = spy(new FrontendService(connHandlerStub));
    }

    @Test
    public void disconnect() throws Exception {
        // 1. Принимаем подключение
        connHandlerStub.getConnections().put(connectionId, messageText);

        // 2. Вручную добавляем сессию (это должен делать AccountService)
        frontendService.addSession(user, connectionId);

        // 3. Делаем дисконнект
        frontendService.disconnect(connectionId);
        verify(frontendService, atLeast(1)).sendCommand(any(ICommand.class));
        verify(frontendService, times(1)).removeSession(user);
        verify(connHandlerStub, times(1)).close(connectionId);
    }

    @Test
    public void addSession() throws Exception {
        Map<UserProfile, ConnectionId> sessions = frontendService.getSessions();
        frontendService.addSession(user, connectionId);
        Assert.assertEquals(1, sessions.size());
        Assert.assertEquals(user, sessions.keySet().iterator().next());
        Assert.assertEquals(connectionId, sessions.values().iterator().next());
    }

    @Test
    public void removeSession() throws Exception {
        Map<UserProfile, ConnectionId> sessions = frontendService.getSessions();
        frontendService.addSession(user, connectionId);
        Assert.assertEquals(1, sessions.size());
        frontendService.removeSession(user);
        Assert.assertEquals(0, sessions.size());
    }

    @Test
    public void findSession() throws Exception {
        frontendService.addSession(user, connectionId);
        Assert.assertEquals(user, frontendService.findSession(connectionId));
    }

    @Test
    public void broadcast() throws Exception {
        connHandlerStub.getConnections().put(connectionId, messageText);

        // 2. Вручную добавляем сессию (это должен делать AccountService)
        frontendService.addSession(user, connectionId);
        frontendService.addSession(new UserProfile("Tester1"), connectionId);
        frontendService.addSession(new UserProfile("Tester2"), connectionId);
        frontendService.addSession(new UserProfile("Tester3"), connectionId);

        String message = "broadcast text";
        frontendService.broadcast(user, message);
        verify(connHandlerStub, times(1)).broadcast(anyListOf(ConnectionId.class), eq(message));
    }

    @Test
    public void send() throws Exception {
        // Принимаем подключение
        connHandlerStub.handleConnectionActivity(true);
        ConnectionId id = connHandlerStub.getConnections().entrySet().iterator().next().getKey();

        frontendService.send(id, messageText);
        Assert.assertEquals(messageText, connHandlerStub.getConnections().get(id));
    }

}
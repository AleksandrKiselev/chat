package server.frontend;

import network.ConnectionId;
import network.IServerConnectionHandler;

import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ServerConnHandlerStub implements IServerConnectionHandler {
    private final ConcurrentHashMap<ConnectionId, String> connections = new ConcurrentHashMap<>();
    private BiFunction<ConnectionId, Boolean, Boolean> onClose;
    private BiFunction<ConnectionId, String, Boolean> onRead;
    private Function<ConnectionId, Boolean> onAccept;
    private Function<Exception, Boolean> onThrow;

    private boolean opened = true;
    private int connCount;
    private String text;

    public ServerConnHandlerStub(int connCount, String text) {
        this.connCount = connCount;
        this.text = text;
    }

    public ConcurrentHashMap<ConnectionId, String> getConnections() {
        return connections;
    }

    @Override
    public void setOnAcceptCallback(Function<ConnectionId, Boolean> onAccept) {
        this.onAccept = onAccept;
    }

    @Override
    public void setOnReadCallback(BiFunction<ConnectionId, String, Boolean> onRead) {
        this.onRead = onRead;
    }

    @Override
    public void setOnCloseCallback(BiFunction<ConnectionId, Boolean, Boolean> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void setOnThrowCallback(Function<Exception, Boolean> onThrow) {
        this.onThrow = onThrow;
    }

    @Override
    public boolean handleConnectionActivity(boolean blocked) {
        if (!opened) return false;

        if (connCount > 0) {
            ConnectionId id = new ConnectionId("StubConnectionId" + connCount);
            connections.put(id, text);
            if (onAccept != null) onAccept.apply(id);
            connCount--;
        }

        for (Map.Entry<ConnectionId, String> e: connections.entrySet()) {
            if (onRead != null) onRead.apply(e.getKey(), e.getValue());
        }

        return true;
    }

    @Override
    public boolean broadcast(List<ConnectionId> ids, String text) {
        return opened;
    }

    @Override
    public boolean send(ConnectionId id, String text) {
        if (connections.containsKey(id))
            connections.put(id, text);
        return opened;
    }

    @Override
    public boolean close(ConnectionId id) {
        opened = false;
        return false;
    }
}

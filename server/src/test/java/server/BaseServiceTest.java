package server;

import command.CommandQueue;
import network.ConnectionId;
import org.junit.After;
import org.junit.Before;
import server.account.AccountService;
import server.account.UserProfile;
import network.SocketConnectionId;
import server.frontend.FrontendService;
import server.message.Message;
import server.message.MessageService;
import service.Address;
import service.ServiceRegistry;

import java.lang.reflect.Field;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;

import static java.net.InetAddress.getLocalHost;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class BaseServiceTest {
    protected final ServiceRegistry serviceRegistry = ServiceRegistry.instance;
    protected final CommandQueue commandQueue = spy(CommandQueue.instance);

    protected final static String username = "Tester";
    protected final static UserProfile user = new UserProfile(username);

    protected final static String messageText = "Test message\n";
    protected final static Message message = new Message(user, messageText);

    protected final Address address1 = new Address();
    protected final Address address2 = new Address();
    protected final Address address3 = new Address();

    protected ConnectionId connectionId = new ConnectionId("testConnectionId");

    protected final AccountService accountServiceMock = mock(AccountService.class);
    protected final FrontendService frontendServiceMock = mock(FrontendService.class);
    protected final MessageService messageServiceMock = mock(MessageService.class);

    @Before
    public void setUp() throws Exception {
        doReturn(AccountService.ACCOUNT_SERVICE_TYPE).when(accountServiceMock).getType();
        doReturn(address1).when(accountServiceMock).getAddress();

        doReturn(FrontendService.FRONTEND_SERVICE_TYPE).when(frontendServiceMock).getType();
        doReturn(address2).when(frontendServiceMock).getAddress();

        doReturn(MessageService.MESSAGE_SERVICE_TYPE).when(messageServiceMock).getType();
        doReturn(address3).when(messageServiceMock).getAddress();

        serviceRegistry.register(accountServiceMock);
        serviceRegistry.register(frontendServiceMock);
        serviceRegistry.register(messageServiceMock);

        commandQueue.removeAll();
    }

    @After
    public void tearDown() throws Exception {
        serviceRegistry.unregisterAll();
        commandQueue.removeAll();
    }
}

package server.account;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class AccountServiceTest {
    private AccountService accountService = new AccountService(1);
    private String registeredUser = "registeredUser";

    @Before
    public void setUp() throws Exception {
        accountService.addUser(registeredUser);
    }

    @Test
    public void isValidUser() throws Exception {
        HashMap<String, Boolean> users = new HashMap<>();
        users.put("A_Kiselev90", true);
        users.put("А_Киселёв90", true);
        users.put("abcdefghijklmnopqrstuvwxyz", true);
        users.put("ABCDEFGHIJKLMNOPQRSTUVWXYZ", true);
        users.put("0123456789", true);
        users.put("0123456789", true);

        users.put("Ёж", false);
        users.put("Bg", false);
        users.put("11", false);
        users.put("A Kiselev", false);
        users.put("A#Kiselev", false);
        users.put("абвгдеёжзиклмнопрстуфхчцшщьыъэюя", false);

        for (Map.Entry<String, Boolean> e: users.entrySet()) {
            Assert.assertEquals(e.getValue(), accountService.isValidUser(e.getKey()));
        }
    }

    @Test
    public void isExistUser() throws Exception {
        Assert.assertTrue(accountService.isExistUser(registeredUser));
        Assert.assertFalse(accountService.isExistUser("NotRegisteredUser"));
    }

    @Test
    public void addAndRemoveUser() throws Exception {
        LinkedList<UserProfile> users = new LinkedList<>();
        int count = 10;
        for (int i = 0; i < count; i++) {
            users.add(accountService.addUser("User" + Integer.toString(i)));
        }

        users.forEach(u -> Assert.assertTrue(accountService.isExistUser(u.getUsername())));
        users.forEach(u -> accountService.removeUser(u));
        users.forEach(u -> Assert.assertFalse(accountService.isExistUser(u.getUsername())));
    }
}
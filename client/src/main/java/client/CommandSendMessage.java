package client;

import command.CommandException;
import service.Address;
import service.ServiceException;

public class CommandSendMessage extends CommandToClient {
    private final String text;

    public CommandSendMessage(Address from, String text) throws ServiceException {
        super(from);
        this.text = text;
    }

    @Override
    public void exec(ClientService clientService) throws ServiceException, CommandException {
        clientService.send(text);
    }

    @Override
    public String toString() {
        return "CommandSendMessage{text='" + text + "}";
    }
}

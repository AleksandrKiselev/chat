package client;

import command.CommandQueue;
import network.TCPSocketClientConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.Address;
import service.ServiceException;
import service.ServiceRegistry;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());
    private static final ServiceRegistry SERVREG = ServiceRegistry.instance;
    private static final CommandQueue CQUEUE = CommandQueue.instance;
    private static final Address CONSOLE_ADDRESS = new Address();

    public static void main(String[] args) {
        try {
            SERVREG.register(
                    new ClientService(
                        new TCPSocketClientConnection(ClientUtils.instance.getHost(), ClientUtils.instance.getPort())
                    ));
            SERVREG.startAll();

            Scanner scanner = new Scanner(System.in);
            System.out.print("Username: ");
            CQUEUE.send(new CommandSendMessage(CONSOLE_ADDRESS, scanner.nextLine()));
            while (true) {
                CQUEUE.send(new CommandSendMessage(CONSOLE_ADDRESS, scanner.nextLine()));
            }
        }

        catch (ServiceException e) {
            SERVREG.stopAll();
            LOGGER.error(e);
            System.out.println("Service error.");
        }

        catch (IOException e) {
            SERVREG.stopAll();
            LOGGER.error(e);
            System.out.println("Connection was closed.");
        }
    }
}

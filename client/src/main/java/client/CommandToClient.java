package client;

import command.CommandException;
import command.CommandTo;
import service.Address;
import service.IService;
import service.ServiceException;

public abstract class CommandToClient extends CommandTo {
    public CommandToClient(Address from) throws ServiceException {
        super(from, ClientService.CLIENT_SERVICE_TYPE);
    }

    @Override
    public void exec(IService service) throws ServiceException, CommandException {
        validateServiceType(service);
        exec((ClientService)service);
    }

    public abstract void exec(ClientService frontendService) throws ServiceException, CommandException;
}

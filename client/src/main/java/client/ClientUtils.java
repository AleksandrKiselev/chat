package client;

import utils.Utils;

public class ClientUtils extends Utils {
    public static final  ClientUtils instance = new ClientUtils();
    private ClientUtils() { super("cfg/client.properties", ""); }

    private int port;
    private String host;

    @Override
    protected void InitializeSettings() {
        host = getProperty("host", "127.0.0.1");
        port = Integer.parseInt(getProperty("port", "8083"));
    }

    @Override
    public String toString() {
        return "port=" + port + ", host='" + host + "'";
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }
}

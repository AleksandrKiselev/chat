package client;

import network.IClientConnectionHandler;
import network.TCPSocketClientConnection;
import network.TCPSocketConnection;
import service.CommandService;
import service.ServiceException;
import service.ServiceType;
import service.worker.ServiceWorker;

import java.io.IOException;

public class ClientService extends CommandService {
    public final static ServiceType CLIENT_SERVICE_TYPE = new ServiceType("ClientService");
    private IClientConnectionHandler connectionHandler;

    public ClientService(IClientConnectionHandler connectionHandler) throws IOException {
        super(CLIENT_SERVICE_TYPE, 1, false);
        this.connectionHandler = connectionHandler;

        getConnectionHandler().setOnReadCallback(this::onReadCallback);
        getConnectionHandler().setOnCloseCallback(this::onCloseCallback);
        getConnectionHandler().setOnThrowCallback(this::onThrowCallback);

        addWorker(new ClientWorker(this), false);
    }

    public IClientConnectionHandler getConnectionHandler() {
        if (connectionHandler == null)
            connectionHandler = new TCPSocketClientConnection(
                    TCPSocketConnection.DEFAULT_HOST,
                    TCPSocketConnection.DEFAULT_PORT
            );
        return connectionHandler;
    }

    protected boolean onReadCallback(String text) {
        System.out.print(text);
        return true;
    }

    protected boolean onCloseCallback(boolean closed) {
        System.out.print("connection was closed");
        return true;
    }

    protected boolean onThrowCallback(Exception e) {
        System.out.print(e.getMessage());
        stop();
        return true;
    }

    public void send(String text) {
        getConnectionHandler().send(text);
    }


    /*------------------------------------------------------------------------------------------------------------
    ClientBot worker
    ------------------------------------------------------------------------------------------------------------*/
    private class ClientWorker extends ServiceWorker {
        public ClientWorker(ClientService clientService) {
            super(clientService.getType().toString() + ":ClientWorker", clientService);
        }

        public ClientService getClientService() {
            return (ClientService)getService();
        }

        @Override
        protected boolean doWork() throws InterruptedException, ServiceException {
            return getClientService().getConnectionHandler().handleConnectionActivity(true);
        }

        @Override
        protected void free() {
            getService().stop();
        }
    }
}

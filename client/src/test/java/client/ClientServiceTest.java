package client;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ClientServiceTest {
    private ClientService clientService;
    private ClientConnHandlerStub connHandlerStub;
    private String text1 = "Hello, chat!";
    private String text2 = "Chat, hello!";
    private int count = 10;

    @Before
    public void setUp() throws Exception {
        connHandlerStub = spy(new ClientConnHandlerStub(text1, count));
        clientService = spy(new ClientService(connHandlerStub));
    }

    @Test
    public void send() throws Exception {
        clientService.send(text2);
        verify(connHandlerStub, times(1)).send(text2);
        Assert.assertEquals(text2, connHandlerStub.getText());
    }
}
package client;

import network.IClientConnectionHandler;

import java.util.function.Function;

public class ClientConnHandlerStub implements IClientConnectionHandler {
    private Function<Exception, Boolean> onThrow;
    private Function<Boolean, Boolean> onClose;
    private Function<String, Boolean> onRead;

    private boolean opened = true;
    private String text;
    private int count;

    public ClientConnHandlerStub(String text, int count) {
        this.text = text;
        this.count = count;
    }

    public String getText() {
        return text;
    }

    @Override
    public void setOnReadCallback(Function<String, Boolean> onRead) {
        this.onRead = onRead;
    }

    @Override
    public void setOnCloseCallback(Function<Boolean, Boolean> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void setOnThrowCallback(Function<Exception, Boolean> onThrow) {
        this.onThrow = onThrow;
    }

    @Override
    public boolean handleConnectionActivity(boolean blocked) {
        if (--count <= 0) opened = false;
        if (!opened) return false;
        if (onRead != null) onRead.apply(text);
        return true;
    }

    @Override
    public boolean send(String text) {
        this.text = text;
        return opened;
    }

    @Override
    public boolean close() {
        opened = false;
        return true;
    }
}

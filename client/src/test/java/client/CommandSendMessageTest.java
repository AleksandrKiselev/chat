package client;

import command.ICommand;
import org.junit.Before;
import org.junit.Test;
import service.Address;
import service.ServiceRegistry;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doReturn;

public class CommandSendMessageTest {
    private ClientService clientService;
    private Address address = new Address();

    @Before
    public void setUp() throws Exception {
        clientService = mock(ClientService.class);

        doReturn(ClientService.CLIENT_SERVICE_TYPE).when(clientService).getType();
        doReturn(address).when(clientService).getAddress();

        ServiceRegistry.instance.register(clientService);
    }

    @Test
    public void exec() throws Exception {
        String messageText = "test message";
        ICommand command = new CommandSendMessage(address, messageText);
        command.exec(clientService);
        verify(clientService, times(1)).send(messageText);
    }
}
import highload.StampedMessage;
import org.junit.Assert;
import org.junit.Test;
import server.message.Message;

public class StampedMessageTest {
    @Test
    public void getUsername() throws Exception {
        String username = "Username111";
        String text = "Hello!\n";
        StampedMessage sendMessage = new StampedMessage(username, text);
        StampedMessage receivedMessage = new StampedMessage(
                String.format(Message.MESSAGE_FORMAT, username, text));

        Assert.assertEquals(sendMessage.getUsername(), receivedMessage.getUsername());
        Assert.assertEquals(sendMessage.getText(), receivedMessage.getText());
    }
}
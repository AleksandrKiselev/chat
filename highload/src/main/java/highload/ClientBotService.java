package highload;

import network.IClientConnectionHandler;
import network.TCPSocketClientConnection;
import network.TCPSocketConnection;
import service.Service;
import service.ServiceException;
import service.ServiceType;
import service.worker.ServiceWorker;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class ClientBotService extends Service {
    public final static String MESSAGE = "Hello my dear friend";
    public final static String NAME = "ClientBotService";
    public final static Pattern PATTERN = Pattern.compile(MESSAGE + "[0-9]+");

    private final static AtomicInteger idCreator = new AtomicInteger();

    private final ConcurrentHashMap<StampedMessage, Long> sendedMessages = new ConcurrentHashMap<>();
    private final Random random = new Random();

    private final String botName;
    private final int minSleepTime;
    private final int maxSleepTime;
    private final int maxMessageCount;

    private IClientConnectionHandler connectionHandler;
    private boolean logined = false;
    private int curMessageCount = -1;

    public ClientBotService(int minSleepTime, int maxSleepTime, int maxMessageCount,
                            IClientConnectionHandler connectionHandler) throws IOException {

        super(new ServiceType(NAME + Integer.toString(idCreator.incrementAndGet())));

        this.botName = getType().toString();
        this.minSleepTime = minSleepTime;
        this.maxSleepTime = maxSleepTime;
        this.maxMessageCount = maxMessageCount;
        this.connectionHandler = connectionHandler;

        getConnectionHandler().setOnReadCallback(this::onReadCallback);
        getConnectionHandler().setOnCloseCallback(this::onCloseCallback);
        getConnectionHandler().setOnThrowCallback(this::onThrowCallback);

        addWorker(new ClientBotWorker(this), false);
    }

    private boolean onReadCallback(String text) {
        if (text.contains("Welcome!"))
            logined = true;
        return true;
    }

    private boolean onCloseCallback(boolean closed) {
        if (closed) getLogger().info("connection was closed");
        return true;
    }

    private boolean onThrowCallback(Exception e) {
        getLogger().error(e);
        return true;
    }

    private int getSleepTime() {
        return random.nextInt(maxSleepTime - minSleepTime) + minSleepTime;
    }

    public String getBotName() {
        return botName;
    }

    public IClientConnectionHandler getConnectionHandler() {
        if (connectionHandler == null) {
            connectionHandler = new TCPSocketClientConnection(
                    TCPSocketConnection.DEFAULT_HOST,
                    TCPSocketConnection.DEFAULT_PORT
            );
        }
        return connectionHandler;
    }

    public ConcurrentHashMap<StampedMessage, Long> getSendedMessages() {
        return sendedMessages;
    }

    public boolean isLogined() {
        return logined;
    }

    /*------------------------------------------------------------------------------------------------------------
        ClientBot worker
        ------------------------------------------------------------------------------------------------------------*/
    private class ClientBotWorker extends ServiceWorker {
        public ClientBotWorker(Service service) {
            super(service.getType().toString() + ":ClientBotWorker", service);
        }

        @Override
        protected boolean doWork() throws InterruptedException, ServiceException {
            if (!getConnectionHandler().handleConnectionActivity(false))
                return false;

            if (curMessageCount >= maxMessageCount) {
                getConnectionHandler().send("!logout\n");
                getConnectionHandler().close();
                logined = false;
                return false;
            }

            if (!logined && curMessageCount == -1) {
                getConnectionHandler().send(botName + "\n");
                curMessageCount++;
                return true;
            }

            if (logined) {
                StampedMessage message = new StampedMessage(botName, MESSAGE + Integer.toString(curMessageCount));
                sendedMessages.put(message, message.getTimestamp());
                curMessageCount++;

                getConnectionHandler().send(message.getText() + "\n");
            }

            Thread.sleep(getSleepTime());
            return true;
        }

        @Override
        protected void free() {
            getService().stop();
        }
    }
}

package highload;

import utils.Utils;

public class HighloadUtils extends Utils {
    public static final HighloadUtils instance = new HighloadUtils();
    private HighloadUtils() { super("cfg/highload.properties", ""); }

    private String host;
    private int port;
    private int botCount;
    private int messageCount;
    private int statMessageCount;
    private int minSleepTime;
    private int maxSleepTime;

    @Override
    protected void InitializeSettings() {
        host = getProperty("host", "127.0.0.1");
        port = Integer.parseInt(getProperty("port", "8083"));

        botCount = Integer.parseInt(getProperty("botCount", "100"));
        messageCount = Integer.parseInt(getProperty("messageCount", "10"));
        minSleepTime = Integer.parseInt(getProperty("minSleepTime", "500"));
        maxSleepTime = Integer.parseInt(getProperty("maxSleepTime", "2000"));

        statMessageCount = Integer.parseInt(getProperty("statMessageCount", "1000"));
    }

    @Override
    public String toString() {
        return "host='" + host + '\'' +
                ", port=" + port +
                ", botCount=" + botCount +
                ", messageCount=" + messageCount +
                ", statMessageCount=" + statMessageCount +
                ", minSleepTime=" + minSleepTime +
                ", maxSleepTime=" + maxSleepTime;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getBotCount() {
        return botCount;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public int getStatMessageCount() {
        return statMessageCount;
    }

    public int getMinSleepTime() {
        return minSleepTime;
    }

    public int getMaxSleepTime() {
        return maxSleepTime;
    }
}

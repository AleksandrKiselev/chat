package highload;

public class StampedMessage {
    private long timestamp = System.currentTimeMillis();
    private final String username;
    private final String text;

    public StampedMessage(String username, String text) {
        this.username = username;
        this.text = text;
    }

    public StampedMessage(String message) {
        // TODO: parsing
        for (;;) {
            int start = message.indexOf('[');
            if (start != 0) break;

            int end = message.indexOf(']');
            if (end < 0) break;

            username = message.substring(start + 1, end);
            text = message.substring(end + 2, message.length());
            return;
        }

        username = "";
        text = message;
    }

    public String getUsername() {
        return username;
    }

    public String getText() {
        return text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StampedMessage message = (StampedMessage) o;

        if (username != null ? !username.equals(message.username)  : message.username != null) return false;
        return text.equals(message.text);

    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + text.hashCode();
        return result;
    }
}

package highload;

import network.IClientConnectionHandler;
import network.TCPSocketClientConnection;
import network.TCPSocketConnection;
import server.account.AccountService;
import service.Service;
import service.ServiceException;
import service.ServiceType;
import service.worker.ServiceWorker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

public class StatBotService extends Service {
    private final LinkedList<ClientBotService> clientBotServices = new LinkedList<>();
    private final HashMap<String, LinkedList<StampedMessage>> receivedMessages = new HashMap<>();

    private final int maxMessageCount;
    private IClientConnectionHandler connectionHandler;
    private int currentMessageCount = 0;

    public StatBotService(IClientConnectionHandler connectionHandler, int maxMessageCount) throws IOException {
        super(new ServiceType("StatBotService"));
        this.maxMessageCount = maxMessageCount;

        this.connectionHandler = connectionHandler;
        getConnectionHandler().setOnReadCallback(this::onReadCallback);
        getConnectionHandler().setOnCloseCallback(this::onCloseCallback);
        getConnectionHandler().setOnThrowCallback(this::onThrowCallback);

        addWorker(new StatBotWorker(this), false);
    }

    public IClientConnectionHandler getConnectionHandler() {
        if (connectionHandler == null) {
            connectionHandler = new TCPSocketClientConnection(
                    TCPSocketConnection.DEFAULT_HOST,
                    TCPSocketConnection.DEFAULT_PORT
            );
        }
        return connectionHandler;
    }

    protected boolean onReadCallback(String text) {
        final long timestamp = System.currentTimeMillis();

        String[] lines = text.split("\n");
        for (String line: lines) {
            Matcher matcher = ClientBotService.PATTERN.matcher(line);
            if (matcher.find()) {
                StampedMessage message = new StampedMessage(line);
                message.setTimestamp(timestamp);
                String name = message.getUsername();

                LinkedList<StampedMessage> messages = receivedMessages.get(name);
                if (messages == null) {
                    messages = new LinkedList<>();
                    receivedMessages.put(name, messages);
                }
                messages.add(message);

                if (!name.isEmpty() && !name.equals(AccountService.SYSTEM_USER.getUsername())) {
                    currentMessageCount++;
                }
            } else {
                getLogger().info(line);
            }
        }

        return true;
    }

    private boolean onCloseCallback(boolean closed) {
        if (closed) getLogger().info("connection was closed");
        return true;
    }

    private boolean onThrowCallback(Exception e) {
        getLogger().error(e);
        return true;
    }

    public void addClientBot(ClientBotService clientBotService) {
        clientBotServices.add(clientBotService);
    }

    public int getLoginedBotsCount() {
        int count = 0;
        for (ClientBotService b: clientBotServices) {
            if (b.isLogined()) count++;
        }
        return count;
    }


    /*------------------------------------------------------------------------------------------------------------
    StatBot worker
    ------------------------------------------------------------------------------------------------------------*/
    private class StatBotWorker extends ServiceWorker {
        public StatBotWorker(Service service) {
            super(service.getType().toString() + ":StatBotService", service);
        }

        @Override
        protected boolean initialize() {
            getConnectionHandler().send("StatBot\n");
            return true;
        }

        @Override
        protected boolean doWork() throws InterruptedException, ServiceException {
            if (!getConnectionHandler().handleConnectionActivity(true)) return false;

            if (currentMessageCount >= maxMessageCount) {
                ArrayList<Long> pings = new ArrayList<>();
                for (ClientBotService clientBot: clientBotServices) {
                    LinkedList<StampedMessage> receivedList = receivedMessages.get(clientBot.getBotName());
                    if (receivedList == null) continue;

                    ConcurrentHashMap<StampedMessage, Long> sendedMessages = clientBot.getSendedMessages();
                    for (StampedMessage receivedMessage: receivedList) {
                        Long sendedTimestamp = sendedMessages.get(receivedMessage);
                        if (sendedTimestamp == null) continue;

                        pings.add(receivedMessage.getTimestamp() - sendedTimestamp);
                    }
                }

                long min = -1;
                long max = -1;
                long sum = 0;

                for (Long ping: pings) {
                    if (min == -1) min = ping;
                    else if (min > ping) min = ping;

                    if (max < ping) max = ping;

                    sum += ping;
                }

                double average = pings.size() > 0 ? sum / pings.size() : 0;

                StringBuilder sb = new StringBuilder();
                sb.append("\n");
                sb.append("==========================SERVER_PING_INFO=========================\n");
                sb.append("\n");
                sb.append("ACTIVE BOT COUNT: " + getLoginedBotsCount());
                sb.append("\n");
                sb.append("MESSAGES COUNT: " + currentMessageCount);
                sb.append("\n");
                sb.append("MINIMUM PING (ms): " + Long.toString(min));
                sb.append("\n");
                sb.append("MAXIMUM PING (ms): " + Long.toString(max));
                sb.append("\n");
                sb.append("AVERAGE PING (ms): " + Double.toString(average));
                sb.append("\n");
                sb.append("\n");

                getLogger().info(sb.toString());

                currentMessageCount = 0;
                receivedMessages.clear();

                getConnectionHandler().send("!exectime\n!info\n");
            }

            return true;
        }

        @Override
        protected void free() {
            getService().stop();
        }
    }
}

package highload;

import network.TCPSocketClientConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.ServiceRegistry;

import java.util.Random;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());
    private static final ServiceRegistry SERVREG = ServiceRegistry.instance;
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        final String host = HighloadUtils.instance.getHost();
        final int port = HighloadUtils.instance.getPort();

        final int botCount = HighloadUtils.instance.getBotCount();
        final int messageCount = HighloadUtils.instance.getMessageCount();
        final int minSleepTime = HighloadUtils.instance.getMinSleepTime();
        final int maxSleepTime = HighloadUtils.instance.getMaxSleepTime();

        final int statMessageCount = HighloadUtils.instance.getStatMessageCount();

        try {
            StatBotService infoBot = new StatBotService(new TCPSocketClientConnection(host, port), statMessageCount);

            for (int i = 0; i < botCount; ++i) {
                ClientBotService clientBot = new ClientBotService(
                        minSleepTime, maxSleepTime, messageCount,
                        new TCPSocketClientConnection(host, port)
                );

                SERVREG.register(clientBot);
                infoBot.addClientBot(clientBot);
            }

            infoBot.start();
            Thread.sleep(100);
            SERVREG.startAllEx(RANDOM.nextInt(100));
        }

        catch (Exception e) {
            SERVREG.stopAll();
            LOGGER.error(e);
        }
    }
}

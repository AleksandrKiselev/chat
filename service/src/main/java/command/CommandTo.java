package command;

import service.*;

public abstract class CommandTo extends Command {
    private final static String ERR_SERVICE_TYPE_IS_INVALID = "Service type is invalid.";
    private final ServiceType toType;

    public CommandTo(Address from, ServiceType toType) throws ServiceException {
        super(from, ServiceRegistry.instance.getAddress(toType));
        this.toType = toType;
    }

    protected void validateServiceType(IService service) throws CommandException {
        if (service.getType() != getToType()) {
            throw new CommandException(ERR_SERVICE_TYPE_IS_INVALID);
        }
    }

    public ServiceType getToType() {
        return toType;
    }
}

package command;

import service.Address;
import service.IService;
import service.ServiceException;

public abstract class Command implements ICommand {
    private final CommandQueue commandQueue = CommandQueue.instance;
    private final Address from;
    private final Address to;

    public Command(Address from, Address to) {
        this.from = from;
        this.to = to;
    }

    protected void sendCommand(ICommand command) {
        commandQueue.send(command);
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public Address getFrom() {
        return from;
    }

    @Override
    public Address getTo() {
        return to;
    }

    @Override
    public abstract void exec(IService service) throws ServiceException, CommandException;
}

package command;

import service.IService;
import service.ServiceException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

public class AsyncCommandExecutor {
    public static final AsyncCommandExecutor instance = new AsyncCommandExecutor();
    private AsyncCommandExecutor() {}

    private final ExecutorService executor = Executors.newCachedThreadPool();

    public void exec(ICommand command,
                     IService service,
                     Function<ServiceException, Boolean> serviceExceptionCallback,
                     Function<CommandException, Boolean> commandExceptionCallback) {

        executor.submit(() -> {
            try {
                command.exec(service);
            }

            catch (ServiceException e) {
                if (serviceExceptionCallback != null)
                    serviceExceptionCallback.apply(e);
            }

            catch (CommandException e) {
                if (commandExceptionCallback != null)
                    commandExceptionCallback.apply(e);
            }
        });
    }
}

package command;

import service.Address;
import utils.DebugUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class CommandQueue {
    public static final CommandQueue instance = new CommandQueue();
    private CommandQueue() {}

    private final ConcurrentHashMap<Address, LinkedBlockingQueue<ICommand>> commands = new ConcurrentHashMap<>();

    private LinkedBlockingQueue<ICommand> getQueue(Address address) {
        LinkedBlockingQueue<ICommand> queue = commands.get(address);
        if (queue == null) {
            queue = new LinkedBlockingQueue<>();
            commands.put(address, queue);
        }
        return queue;
    }

    public boolean send(ICommand command) {
        command = new TimestampCommandDecorator(command);
        if (DebugUtils.instance.isTraceCommands()) {
            command = new TraceCommandDecorator(command);
        }

        LinkedBlockingQueue<ICommand> queue = getQueue(command.getTo());
        return queue.offer(command);
    }

    public ICommand receive(Address address, boolean blocked) throws InterruptedException {
        LinkedBlockingQueue<ICommand> queue = getQueue(address);
        return blocked ? queue.take() : queue.poll();
    }

    public int size(Address address) {
        return getQueue(address).size();
    }

    public void remove(Address address) {
        commands.clear();
    }

    public void removeAll() {
        commands.clear();
    }
}

package command;

import org.apache.logging.log4j.LogManager;
import service.IService;
import utils.CommandUtils;
import utils.DebugUtils;

public class TimestampCommandDecorator extends CommandDecorator {
    private final long timestamp = System.currentTimeMillis();

    public TimestampCommandDecorator(ICommand command) {
        super(command);
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    protected void afterExec(IService service) {
        long execTime = System.currentTimeMillis() - getTimestamp();

        /*if (execTime > CommandUtils.instance.getMaxExecTime()) {
            if (service.canAddWorker()) {
                service.addWorker(new CommandWorker((Service) service), true);
            }
        }*/

        CommandUtils.instance.addExecTime(getName(), execTime);
        if (DebugUtils.instance.isTraceExecTime()) {
            LogManager.getLogger(getName()).info("exec time (ms): " + Long.toString(execTime));
        }
    }
}

package command;

import service.Address;
import service.IService;
import service.ServiceException;

public interface ICommand {
    Address getFrom();
    Address getTo();
    String getName();
    void exec(IService service) throws ServiceException, CommandException;
}

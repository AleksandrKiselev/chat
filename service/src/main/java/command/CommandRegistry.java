package command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommandRegistry {
    public static final CommandRegistry instance = new CommandRegistry();
    private CommandRegistry() {}

    private static final Logger logger = LogManager.getLogger(CommandRegistry.class.getName());
    private final HashMap<String, Class> commands = new HashMap<>();
    private final HashMap<String, String> helps = new HashMap<>();

    public void registry(String id, Class command, String help) {
        commands.put(id, command);
        helps.put(id, help);
    }

    public void unregistry(String id) {
        commands.remove(id);
        helps.remove(id);
    }

    public ICommand getCommand(String id, Object... args) {
        ICommand command = null;
        try {
            Class ref = commands.get(id);
            if (ref == null) return null;

            ArrayList<Class> classes = new ArrayList<>();
            for (Object o : args) {
                classes.add(o.getClass());
            }

            Class arr[] = new Class[classes.size()];
            arr = classes.toArray(arr);
            Constructor co = ref.getConstructor(arr);
            command = (ICommand) co.newInstance(args);

        } catch(Exception e) {
            logger.error(e);
        }
        return command;
    }

    public String getHelp() {
        String help = "Commands list: \n";
        for (Map.Entry<String, String> e: helps.entrySet()) {
            help += e.getKey() + " - " + e.getValue() + "\n";
        }
        return help;
    }
}

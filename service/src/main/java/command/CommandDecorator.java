package command;

import service.Address;
import service.IService;
import service.ServiceException;

public abstract class CommandDecorator implements ICommand {
    private final ICommand command;

    public CommandDecorator(ICommand command) {
        this.command = command;
    }

    public ICommand getCommand() {
        return command;
    }

    @Override
    public String getName() {
        return command.getName();
    }

    @Override
    public Address getFrom() {
        return command.getFrom();
    }

    @Override
    public Address getTo() {
        return command.getTo();
    }

    protected void beforeExec(IService service) { }

    protected void afterExec(IService service) { }

    @Override
    public void exec(IService service) throws ServiceException, CommandException {
        beforeExec(service);
        command.exec(service);
        afterExec(service);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

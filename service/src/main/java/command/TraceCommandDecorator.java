package command;

import org.apache.logging.log4j.LogManager;
import service.IService;

public class TraceCommandDecorator extends CommandDecorator {
    public TraceCommandDecorator(ICommand command) {
        super(command);
    }

    @Override
    protected void beforeExec(IService service) {
        LogManager.getLogger(service.getType().toString()).info("exec " + getCommand());
    }
}

package service;

import utils.DebugUtils;

import java.util.HashMap;
import java.util.Map;

public class ServiceRegistry {
    public static final  ServiceRegistry instance = new ServiceRegistry();
    private ServiceRegistry() {}

    private final static String ERR_SERVICE_ALREADY_REGISTERED = "Service is already registered.";
    private final static String ERR_SERVICE_NOT_REGISTERED = "This type of service is not registered.";
    private final HashMap<ServiceType, IService> services = new HashMap<>();

    public boolean isRegistered(IService service) {
        return isRegistered(service.getType());
    }

    public boolean isRegistered(ServiceType type) {
        return services.containsKey(type);
    }

    public void register(IService service) throws ServiceException {
        if (isRegistered(service)) {
            throw new ServiceException(service.getType(), ERR_SERVICE_ALREADY_REGISTERED);
        }
        if (DebugUtils.instance.isTraceService()) {
            service = new TraceServiceDecorator(service);
        }
        services.put(service.getType(), service);
    }

    public void unregister(ServiceType type) throws ServiceException {
        if (!isRegistered(type)) {
            throw new ServiceException(type, ERR_SERVICE_NOT_REGISTERED);
        }
        services.remove(type);
    }

    public void unregisterAll() {
        services.clear();
    }

    public IService getService(ServiceType type) throws ServiceException {
        if (!isRegistered(type)) {
            throw new ServiceException(type, ERR_SERVICE_NOT_REGISTERED);
        }
        return services.get(type);
    }

    public Address getAddress(ServiceType type) throws ServiceException {
        IService service = getService(type);
        if (service == null) {
            throw new ServiceException(type, ERR_SERVICE_NOT_REGISTERED);
        }
        return service.getAddress();
    }

    public void startAll() {
        services.forEach((k, v) -> v.start());
    }

    public void startAllEx(int sleepTime) {
        for (Map.Entry<ServiceType, IService> entry: services.entrySet()) {
            entry.getValue().start();
            try { Thread.sleep(sleepTime); }
            catch (InterruptedException e) { continue; }
        }
    }

    public void stopAll() {
        services.forEach((k, v) -> v.stop());
    }
}

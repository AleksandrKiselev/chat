package service;

import service.worker.IServiceWorker;
import utils.CommandUtils;

public class TraceServiceDecorator implements IService {
    private final IService service;

    public TraceServiceDecorator(IService service) {
        this.service = service;
    }

    public Service getService() {
        return (Service) service; // TODO:
    }

    @Override
    public Address getAddress() {
        return getService().getAddress();
    }

    @Override
    public ServiceType getType() {
        return getService().getType();
    }

    @Override
    public boolean addWorker(IServiceWorker worker, boolean start) {
        boolean result = getService().addWorker(worker, start);
        if (result) {
            getService().getLogger().info("Add new worker " +
                    worker.getClass().getSimpleName() +
                    ". Current count: " + getService().getWorkersCount());
        } else {
            getService().getLogger().info("Can't add new worker. Limit: " +
                    CommandUtils.instance.getMaxWorkers() +
                    ". Current count: " + getService().getWorkersCount());
        }
        return result;
    }

    @Override
    public boolean canAddWorker() {
        return getService().canAddWorker();
    }

    @Override
    public void start() {
        getService().getLogger().info("service started.");
        getService().start();
    }

    @Override
    public void stop() {
        getService().stop();
        getService().getLogger().info("service stopped.");
    }
}

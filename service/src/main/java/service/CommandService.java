package service;

import service.worker.CommandWorker;

public class CommandService extends Service {
    public CommandService(ServiceType type, int workersCount, boolean asyncCommand) {
        super(type);

        for (int i = 0; i < workersCount; i++) {
            addWorker(new CommandWorker(this, asyncCommand), false);
        }
    }
}

package service;

import java.util.concurrent.atomic.AtomicInteger;

public class Address {
    private static AtomicInteger idCreator = new AtomicInteger();
    private final int serviceId;

    public Address() {
        serviceId = idCreator.incrementAndGet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return serviceId == address.serviceId;
    }

    @Override
    public int hashCode() {
        return serviceId;
    }
}

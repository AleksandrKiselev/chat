package service;

import service.worker.IServiceWorker;

public interface IService {
    Address getAddress();
    ServiceType getType();

    boolean addWorker(IServiceWorker worker, boolean start);
    boolean canAddWorker();

    void start();
    void stop();
}

package service;

import command.CommandQueue;
import command.ICommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.worker.IServiceWorker;
import service.worker.ServiceWorker;
import service.worker.TraceWorkerDecorator;
import utils.CommandUtils;
import utils.DebugUtils;

import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class Service implements IService {
    private final ConcurrentLinkedQueue<IServiceWorker> workers = new ConcurrentLinkedQueue<>();
    private final CommandQueue commandQueue = CommandQueue.instance;
    private final Address address = new Address();

    private final ServiceType type;
    private final Logger logger;

    public Service(ServiceType type) {
        this.logger = LogManager.getLogger(type.toString());
        this.type = type;
    }

    public void sendCommand(ICommand command) {
        commandQueue.send(command);
    }

    public ICommand receiveCommand(boolean blocked) throws InterruptedException {
        return commandQueue.receive(address, blocked);
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public ServiceType getType() {
        return type;
    }

    public ConcurrentLinkedQueue<IServiceWorker> getWorkers() {
        return workers;
    }

    public int getWorkersCount() {
        return getWorkers().size();
    }

    @Override
    public boolean canAddWorker() { return getWorkersCount() <= CommandUtils.instance.getMaxWorkers(); }

    @Override
    public boolean addWorker(IServiceWorker worker, boolean start) {
        if (canAddWorker()) {
            if (DebugUtils.instance.isTraceWorker()) {
                worker = new TraceWorkerDecorator(worker);
            }
            getWorkers().offer(worker);
            if (start) worker.start();
            return true;
        }
        return false;
    }

    @Override
    public void start() {
        getWorkers().forEach(w -> w.start());
    }

    @Override
    public void stop() {
        getWorkers().forEach(w -> w.interrupt());
    }
}

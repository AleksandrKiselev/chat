package service.worker;

public interface IServiceWorker {
    void start();
    void interrupt();
}

package service.worker;

import service.Service;
import service.ServiceException;

public class ServiceWorker extends Thread implements IServiceWorker {
    private final Service service;

    public ServiceWorker(String name, Service service) {
        super(name);
        this.service = service;
    }

    public Service getService() {
        return service;
    }

    protected boolean initialize() {
        return true;
    }

    protected boolean doWork() throws InterruptedException, ServiceException {
        return true;
    }

    protected boolean handleServiceException(ServiceException e) {
        getService().getLogger().error(e);
        return true;
    }

    protected void free() { }

    @Override
    public void run() {
        if (!initialize()) return;

        while(!Thread.interrupted()) {
            try { if (!doWork()) break; }

            catch (InterruptedException e) { continue; }
            catch (ServiceException e) { if (!handleServiceException(e)) break; }
            catch (Exception e) { getService().getLogger().error(e); }
        }

        free();
    }
}

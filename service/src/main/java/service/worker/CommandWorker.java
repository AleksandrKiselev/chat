package service.worker;

import command.AsyncCommandExecutor;
import command.CommandException;
import command.ICommand;
import service.Service;
import service.ServiceException;

public class CommandWorker extends ServiceWorker {
    private final boolean async;

    public CommandWorker(Service service, boolean async) {
        super(service.getType().toString() + ":CommandWorker", service);
        this.async = async;
    }

    protected boolean isAsync() {
        return async;
    }

    protected boolean commandExecute(boolean blocked, boolean async) throws ServiceException, CommandException, InterruptedException {
        ICommand command = getService().receiveCommand(blocked);
        if (command != null) {
            if (async) {
                AsyncCommandExecutor.instance.exec(
                        command,
                        getService(),
                        this::handleServiceException,
                        this::handleCommandException
                );
            } else { command.exec(getService()); }
        }
        return true;
    }

    protected boolean handleCommandException(CommandException e) {
        getService().getLogger().error(e);
        return true;
    }

    @Override
    protected boolean doWork() throws InterruptedException, ServiceException {
        boolean result = true;
        try { if (!commandExecute(true, isAsync())) result = false; }
        catch (CommandException e) { if (!handleCommandException(e)) result = false; }
        return result;
    }
}

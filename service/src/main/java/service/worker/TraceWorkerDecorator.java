package service.worker;

public class TraceWorkerDecorator implements IServiceWorker {
    private IServiceWorker worker;

    public TraceWorkerDecorator(IServiceWorker worker) {
        this.worker = worker;
    }

    public ServiceWorker getWorker() {
        return (ServiceWorker)worker; // TODO:
    }

    @Override
    public void start() {
        getWorker().getService().getLogger().info(getWorker().getName() + " started.");
        getWorker().start();
    }

    @Override
    public void interrupt() {
        getWorker().getService().getLogger().info(getWorker().getName() + " stopped.");
        getWorker().interrupt();
    }
}

package service;

public class ServiceException extends Exception {
    private ServiceType type = null;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(ServiceType type, String message) {
        super(message);
        this.type = type;
    }

    public ServiceType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        String result = "";
        if (type != null) {
            result += "[" + getType().toString() + "] ";
        }
        result += super.getMessage();
        return result;
    }
}

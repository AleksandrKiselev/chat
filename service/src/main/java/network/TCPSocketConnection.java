package network;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public abstract class TCPSocketConnection {
    public static final String DEFAULT_HOST = "127.0.0.1";
    public static final int DEFAULT_MAX_MESSAGE_SIZE = 5120;
    public static final int DEFAULT_BUFFER_SIZE = 1024;
    public static final int DEFAULT_PORT = 8083;

    private final int port;
    private ByteBuffer buffer;
    private Selector selector;

    public TCPSocketConnection(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public ByteBuffer getBuffer() {
        if (buffer == null) {
            buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
        }
        return buffer;
    }

    public Selector getSelector() throws IOException {
        if (selector == null) {
            selector = Selector.open();
        }
        return selector;
    }

    public SocketChannel getSocket(SelectionKey key) throws IOException {
        if (key == null) return null;
        if (!key.isValid()) return null;

        SocketChannel socket = null;
        if (key.isAcceptable()) {
            socket = ((ServerSocketChannel) key.channel()).accept();
        } else if (key.isReadable() || key.isWritable()) {
            socket = (SocketChannel) key.channel();
        }

        return socket;
    }

    protected String read(SocketChannel socket) throws IOException {
        if (socket == null) return "";

        StringBuilder builder = new StringBuilder();

        int readedSize = 0;
        int messageSize = 0;
        while (messageSize < DEFAULT_MAX_MESSAGE_SIZE &&
                (readedSize = socket.read(getBuffer())) > 0) {

            messageSize += readedSize;

            getBuffer().flip();
            byte[] bytes = new byte[getBuffer().limit()];
            getBuffer().get(bytes);
            builder.append(new String(bytes));
            getBuffer().clear();
        }

        return builder.toString();
    }
}

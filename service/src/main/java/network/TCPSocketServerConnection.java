package network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TCPSocketServerConnection extends TCPSocketConnection implements IServerConnectionHandler {
    private final ConcurrentHashMap<ConnectionId, SocketChannel> connections = new ConcurrentHashMap<>();

    private BiFunction<ConnectionId, Boolean, Boolean> onClose;
    private BiFunction<ConnectionId, String, Boolean> onRead;
    private Function<ConnectionId, Boolean> onAccept;
    private Function<Exception, Boolean> onThrow;

    private ServerSocketChannel serverSocket;

    public TCPSocketServerConnection(int port) {
        super(port);
    }

    public ServerSocketChannel getServerSocket() throws IOException {
        if (serverSocket == null) {
            serverSocket = ServerSocketChannel.open();
            serverSocket.bind(new InetSocketAddress(getPort()));
            serverSocket.configureBlocking(false);
            serverSocket.register(getSelector(), SelectionKey.OP_ACCEPT);
        }
        return serverSocket;
    }

    public ConcurrentHashMap<ConnectionId, SocketChannel> getConnections() {
        return connections;
    }

    public void addConnection(ConnectionId id, SocketChannel socket) {
        getConnections().put(id, socket);
    }

    public SocketChannel findConnection(ConnectionId id) {
        return getConnections().get(id);
    }

    public void removeConnection(ConnectionId id) {
        getConnections().remove(id);
    }

    protected void accept(SelectionKey key) throws IOException {
        SocketChannel socket = getSocket(key);
        socket.configureBlocking(false);

        SocketConnectionId id = new SocketConnectionId(socket);
        socket.register(getSelector(), SelectionKey.OP_READ, id);
        addConnection(id, socket);

        if (onAccept != null) onAccept.apply(id);
    }

    protected void read(SelectionKey key) throws IOException {
        SocketConnectionId id = (SocketConnectionId)key.attachment();
        if (id == null) return;

        String text = read(getSocket(key));

        if (onRead != null) onRead.apply(id, text);
    }

    @Override
    public boolean handleConnectionActivity(boolean blocked) {
        try {
            ServerSocketChannel serverSocket = getServerSocket();
            if (!serverSocket.isOpen()) return false;
        } catch (IOException e) {
            if (onThrow != null) onThrow.apply(e);
            return false;
        }

        Selector selector;
        try {
            selector = getSelector();
            int select = blocked ? selector.select() : selector.selectNow();
            if (select == 0) return true;
        } catch (IOException e) {
            if (onThrow != null) onThrow.apply(e);
            return false;
        }

        Set<SelectionKey> keys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            SelectionKey key = iterator.next();
            try {
                if (!key.isValid()) continue;
                if (key.isAcceptable()) accept(key);
                if (key.isReadable()) read(key);
            }

            catch (IOException e) {
                ConnectionId id = (ConnectionId) key.attachment();
                if (id != null) {
                    boolean closed = close(id);
                    if (onClose != null) onClose.apply(id, closed);
                }
            }

            catch (Exception e) {
                if (onThrow != null) onThrow.apply(e);
            }
        }
        keys.clear();
        return true;
    }

    @Override
    public boolean broadcast(List<ConnectionId> ids, String text) {
        for (ConnectionId id: ids) {
            if (!getConnections().containsKey(id)) continue;
            send(id, text);
        }
        return true;
    }

    @Override
    public boolean send(ConnectionId id, String text) {
        try {
            ByteBuffer msgBuf = ByteBuffer.wrap(text.getBytes());
            SocketChannel socket = findConnection(id);
            if (socket == null) return false;

            socket.write(msgBuf);

        } catch (IOException e) {
            boolean closed = close(id);
            if (onClose != null) onClose.apply(id, closed);
            return false;
        }
        return true;
    }

    @Override
    public boolean close(ConnectionId id) {
        SocketChannel socket = findConnection(id);
        boolean result = true;
        if (socket != null) {
            removeConnection(id);

            try { socket.close(); }
            catch (IOException e) {
                result = false;
                if (onThrow != null) onThrow.apply(e);
            }
        }
        return result;
    }


    @Override
    public void setOnAcceptCallback(Function<ConnectionId, Boolean> onAccept) {
        this.onAccept = onAccept;
    }

    @Override
    public void setOnReadCallback(BiFunction<ConnectionId, String, Boolean> onRead) {
        this.onRead = onRead;
    }

    @Override
    public void setOnCloseCallback(BiFunction<ConnectionId, Boolean, Boolean> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void setOnThrowCallback(Function<Exception, Boolean> onThrow) {
        this.onThrow = onThrow;
    }
}

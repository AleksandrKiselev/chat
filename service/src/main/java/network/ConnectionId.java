package network;

public class ConnectionId {
    protected final String id;

    public ConnectionId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionId)) return false;
        ConnectionId connectionId = (ConnectionId) o;
        return id != null ? id.equals(connectionId.id) : connectionId.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return id;
    }
}

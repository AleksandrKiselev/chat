package network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Function;

public class TCPSocketClientConnection extends TCPSocketConnection implements IClientConnectionHandler {
    private Function<Exception, Boolean> onThrow;
    private Function<Boolean, Boolean> onClose;
    private Function<String, Boolean> onRead;

    private SocketChannel socket;
    private final String host;

    public TCPSocketClientConnection(String host, int port) {
        super(port);
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public SocketChannel getClientSocket() throws IOException {
        if (socket == null) {
            this.socket = SocketChannel.open();
            this.socket.connect(new InetSocketAddress(getHost(), getPort()));
            this.socket.configureBlocking(false);
            this.socket.register(getSelector(), SelectionKey.OP_READ);
        }
        return socket;
    }

    @Override
    public boolean handleConnectionActivity(boolean blocked) {
        try {
            SocketChannel socket = getClientSocket();
            if (!socket.isOpen()) return false;
        } catch (IOException e) {
            if (onThrow != null) onThrow.apply(e);
            return false;
        }

        Selector selector;
        try {
            selector = getSelector();
            int select = blocked ? selector.select() : selector.selectNow();
            if (select == 0) return true;
        } catch (IOException e) {
            if (onThrow != null) onThrow.apply(e);
            return false;
        }

        Set<SelectionKey> keys = selector.selectedKeys();
        Iterator<SelectionKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            SelectionKey key = iterator.next();
            try {
                if (!key.isValid()) continue;
                if (key.isReadable()) read(key);
            }

            catch (IOException e) {
                boolean closed = close();
                if (onClose != null) onClose.apply(closed);
                return false;
            }

            catch (Exception e) {
                if (onThrow != null) onThrow.apply(e);
            }
        }
        keys.clear();
        return true;
    }

    protected void read(SelectionKey key) throws IOException {
        String text = read((SocketChannel)key.channel());
        if (onRead != null) onRead.apply(text);
    }

    @Override
    public boolean send(String text) {
        try {
            ByteBuffer msgBuf = ByteBuffer.wrap(text.getBytes());
            getClientSocket().write(msgBuf);
        } catch (IOException e) {
            boolean closed = close();
            if (onClose != null) onClose.apply(closed);
            return false;
        }
        return true;
    }

    @Override
    public boolean close() {
        boolean result = false;
        try {
            getClientSocket().close();
            result = true;
        } catch (IOException e) {
            if (onThrow != null) onThrow.apply(e);
        }
        return result;
    }

    @Override
    public void setOnReadCallback(Function<String, Boolean> onRead) {
        this.onRead = onRead;
    }

    @Override
    public void setOnCloseCallback(Function<Boolean, Boolean> onClose) {
        this.onClose = onClose;
    }

    @Override
    public void setOnThrowCallback(Function<Exception, Boolean> onThrow) {
        this.onThrow = onThrow;
    }
}

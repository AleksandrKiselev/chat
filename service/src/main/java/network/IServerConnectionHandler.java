package network;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public interface IServerConnectionHandler {
    void setOnAcceptCallback(Function<ConnectionId, Boolean> onAccept);
    void setOnReadCallback(BiFunction<ConnectionId, String, Boolean> onRead);
    void setOnCloseCallback(BiFunction<ConnectionId, Boolean, Boolean> onClose);
    void setOnThrowCallback(Function<Exception, Boolean> onThrow);

    boolean handleConnectionActivity(boolean blocked);
    boolean broadcast(List<ConnectionId> ids, String text);
    boolean send(ConnectionId id, String text);
    boolean close(ConnectionId id);
}

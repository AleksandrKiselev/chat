package network;

import java.nio.channels.SocketChannel;

public class SocketConnectionId extends ConnectionId {
    public SocketConnectionId(SocketChannel sc) {
        super(sc.socket().getInetAddress() + ":" + sc.socket().getPort());
    }
}

package network;

import java.util.function.Function;

public interface IClientConnectionHandler {
    void setOnReadCallback(Function<String, Boolean> onRead);
    void setOnCloseCallback(Function<Boolean, Boolean> onClose);
    void setOnThrowCallback(Function<Exception, Boolean> onThrow);

    boolean handleConnectionActivity(boolean blocked);
    boolean send(String text);
    boolean close();
}

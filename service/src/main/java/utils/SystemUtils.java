package utils;

import com.sun.management.OperatingSystemMXBean;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.text.NumberFormat;
import java.util.Date;

public class SystemUtils extends Utils {
    public static final SystemUtils instance = new SystemUtils();
    private SystemUtils() { super("cfg/service.properties", "system"); }

    private Runtime runtime = Runtime.getRuntime();

    public String GetInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("============================SERVER_INFO============================\n");
        sb.append("Time: ");
        sb.append(new Date().toString());
        sb.append("\n");
        sb.append(this.OsInfo());
        sb.append("\n");
        sb.append(this.CpuInfo());
        sb.append("\n");
        sb.append(this.MemInfo());
        sb.append("\n");
        sb.append("\n");
        return sb.toString();
    }

    private String OSname() {
        return System.getProperty("os.name");
    }

    private String OSversion() {
        return System.getProperty("os.version");
    }

    private String OsArch() {
        return System.getProperty("os.arch");
    }

    private String CpuInfo() {
        OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
                OperatingSystemMXBean.class);

        NumberFormat format = NumberFormat.getInstance();
        StringBuilder sb = new StringBuilder();
        sb.append("CPU load JVM (%): ");
        sb.append(format.format(osBean.getProcessCpuLoad() * 100));
        sb.append("\n");
        sb.append("CPU load system (%): ");
        sb.append(format.format(osBean.getSystemCpuLoad() * 100));
        sb.append("\n");
        return sb.toString();
    }

    private String MemInfo() {
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        NumberFormat format = NumberFormat.getInstance();
        StringBuilder sb = new StringBuilder();
        sb.append("Free memory (MB): ");
        sb.append(format.format(freeMemory / 1024 / 1024));
        sb.append("\n");
        sb.append("Allocated memory (MB): ");
        sb.append(format.format(allocatedMemory / 1024 / 1024));
        sb.append("\n");
        sb.append("Max memory (MB): ");
        sb.append(format.format(maxMemory / 1024 / 1024));
        sb.append("\n");
        sb.append("Total free memory (MB): ");
        sb.append(format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024 / 1024));
        sb.append("\n");

        return sb.toString();

    }

    private String OsInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("OS: ");
        sb.append(this.OSname());
        sb.append("\n");
        sb.append("Version: ");
        sb.append(this.OSversion());
        sb.append("\n");
        sb.append("Architecture: ");
        sb.append(this.OsArch());
        sb.append("\n");
        sb.append("Available processors (cores): ");
        sb.append(runtime.availableProcessors());
        sb.append("\n");
        return sb.toString();
    }

    private String DiskInfo() {
            /* Get a list of all filesystem roots on this system */
        File[] roots = File.listRoots();
        StringBuilder sb = new StringBuilder();

            /* For each filesystem root, print some info */
        for (File root : roots) {
            sb.append("File system root: ");
            sb.append(root.getAbsolutePath());
            sb.append("\n");
            sb.append("Total space (MB): ");
            sb.append(root.getTotalSpace() / 1024 / 1024);
            sb.append("\n");
            sb.append("Free space (MB): ");
            sb.append(root.getFreeSpace() / 1024 / 1024);
            sb.append("\n");
            sb.append("Usable space (MB): ");
            sb.append(root.getUsableSpace() / 1024 / 1024);
            sb.append("\n");
        }
        return sb.toString();
    }
}

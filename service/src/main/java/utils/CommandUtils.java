package utils;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CommandUtils extends Utils {
    public static final CommandUtils instance = new CommandUtils();
    private CommandUtils() { super("cfg/service.properties", "command"); }

    private final ConcurrentHashMap<String, ConcurrentLinkedQueue<Long>> commandExecTimes = new ConcurrentHashMap<>();

    private int maxExecTime;
    private int maxWorkers;

    @Override
    protected void InitializeSettings() {
        maxExecTime = Integer.parseInt(getProperty("maxExecTime", "1000"));
        maxWorkers = Integer.parseInt(getProperty("maxWorkers", "1000"));
    }

    @Override
    public String toString() {
        return "maxExecTime=" + maxExecTime + ", maxWorkers=" + maxWorkers;
    }

    public int getMaxExecTime() {
        return maxExecTime;
    }

    public int getMaxWorkers() {
        return maxWorkers;
    }

    public void addExecTime(String commandName, long execTime) {
        ConcurrentLinkedQueue<Long> execTimes = commandExecTimes.get(commandName);
        if (execTimes == null) {
            execTimes = new ConcurrentLinkedQueue<>();
            commandExecTimes.put(commandName, execTimes);
        }
        execTimes.add(execTime);
    }

    public String getExecTimeInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("========================EXECUTION_TIME_INFO========================\n");

        List<String> commands = Collections.list(commandExecTimes.keys());
        for (String command: commands) {
            ConcurrentLinkedQueue<Long> execTimes = commandExecTimes.get(command);
            if (execTimes == null) continue;

            long max = -1;
            long min = -1;
            long sum = 0;
            long count = 0;

            for (Long execTime: execTimes) {
                if (min == -1) min = execTime;
                else if (min > execTime) min = execTime;

                if (max < execTime) max = execTime;

                sum += execTime;
                count++;
            }

            double average = execTimes.size() > 0 ? sum / execTimes.size() : 0;

            sb.append("\n");
            sb.append("COMMAND " + command);
            sb.append("\n");
            sb.append("COUNT: " + Long.toString(count));
            sb.append("\n");
            sb.append("MIN (ms): " + Long.toString(min));
            sb.append("\n");
            sb.append("MAX (ms): " + Long.toString(max));
            sb.append("\n");
            sb.append("AVERAGE (ms): " + Double.toString(average));
            sb.append("\n");
            sb.append("\n");
        }

        return sb.toString();
    }
}

package utils;

public class DebugUtils extends Utils {
    public static final DebugUtils instance = new DebugUtils("cfg/service.properties", "debug");
    private DebugUtils(String fileName, String prefix) { super(fileName, prefix); }

    private boolean traceCommands;
    private boolean traceExecTime;
    private boolean traceQueueSize;
    private boolean traceService;
    private boolean traceWorker;

    public boolean isTraceCommands() {
        return traceCommands;
    }

    public boolean isTraceExecTime() {
        return traceExecTime;
    }

    public boolean isTraceQueueSize() {
        return traceQueueSize;
    }

    public boolean isTraceService() {
        return traceService;
    }

    public boolean isTraceWorker() {
        return traceWorker;
    }

    @Override
    protected void InitializeSettings() {
        traceCommands = Boolean.parseBoolean(getProperty("traceCommands", "false"));
        traceExecTime = Boolean.parseBoolean(getProperty("traceExecTime", "false"));
        traceQueueSize = Boolean.parseBoolean(getProperty("traceQueueSize", "false"));
        traceService = Boolean.parseBoolean(getProperty("traceService", "false"));
        traceWorker = Boolean.parseBoolean(getProperty("traceWorker", "false"));
    }

    @Override
    public String toString() {
        return "traceCommands=" + traceCommands +
                ", traceExecTime=" + traceExecTime +
                ", traceQueueSize=" + traceQueueSize +
                ", traceService=" + traceService +
                ", traceWorker=" + traceWorker;
    }
}

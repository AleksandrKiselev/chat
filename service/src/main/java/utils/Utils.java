package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class Utils {
    private final Properties properties = new Properties();
    private final Logger logger;
    private final String prefix;

    public Utils(String fileName, String prefix) {
        this.logger = LogManager.getLogger(this.getClass().getSimpleName());
        this.prefix = prefix;
        try {
            InputStream input = new FileInputStream(fileName);
            properties.load(input);
        } catch (IOException e) {
            getLogger().error(fileName + " not found.");
        }
        InitializeSettings();

        String printStr = toString();
        if (!printStr.isEmpty()) getLogger().info(printStr);
    }

    protected void InitializeSettings() { }

    protected Logger getLogger() {
        return logger;
    }

    protected final Properties getProperties() {
        return properties;
    }

    protected String getProperty(String key, String defaultValue) {
        return getProperties().getProperty(getPropertyName(key), defaultValue);
    }

    protected String getPrefix() {
        return prefix;
    }

    protected String getPropertyName(String property) {
        String result = getPrefix();
        if (!result.isEmpty()) result += ".";
        result += property;
        return result;
    }

    @Override
    public String toString() {
        return "";
    }
}

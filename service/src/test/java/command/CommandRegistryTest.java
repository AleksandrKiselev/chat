package command;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.Address;


public class CommandRegistryTest {
    private CommandRegistry commandRegistry = CommandRegistry.instance;

    @Before
    public void setUp() throws Exception {
        commandRegistry.registry("!stub", StubCommand.class, "Пустая команда");
    }

    @After
    public void tearDown() throws Exception {
        commandRegistry.unregistry("!stub");
    }

    @Test
    public void getCommand() throws Exception {
        for (int i = 0; i < 5; i++) {
            String text = "Command" + Integer.toString(i);
            Address from = new Address();
            Address to = new Address();

            ICommand newCommand = commandRegistry.getCommand("!stub", from, to, text);
            Assert.assertNotNull(newCommand);

            try {
                StubCommand stubCommand = (StubCommand) newCommand;
                Assert.assertEquals(text, stubCommand.text);
                Assert.assertEquals(from, stubCommand.getFrom());
                Assert.assertEquals(to, stubCommand.getTo());
            } catch (Exception e) {
                Assert.fail(e.getMessage());
            }
        }
    }
}
package command;

import service.Address;
import service.IService;
import service.ServiceException;

public class StubCommand extends Command {
    public final String text;
    public StubCommand(Address from, Address to, String text) {
        super(from, to);
        this.text = text;
    }
    @Override
    public void exec(IService service) throws ServiceException, CommandException {
    }
}
package command;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.Address;
import service.IService;
import service.ServiceException;

import java.util.Random;

public class CommandQueueTest {
    private Random random = new Random();
    private CommandQueue commandQueue = CommandQueue.instance;
    private Address address1 = new Address();
    private Address address2 = new Address();

    private Command command1 = new Command(address1, address2) {
        @Override
        public void exec(IService service) throws ServiceException {
        }
    };
    private Command command2 = new Command(address2, address1) {
        @Override
        public void exec(IService service) throws ServiceException {
        }
    };

    @Before
    public void setUp() throws Exception {
        commandQueue.removeAll();
    }

    @After
    public void tearDown() throws Exception {
        commandQueue.removeAll();
    }

    @Test
    public void size() {
        int oldCount = commandQueue.size(command1.getTo());
        int count = random.nextInt(100);
        for (int i = 0; i < count; i++) {
            commandQueue.send(command1);
        }
        Assert.assertEquals(commandQueue.size(command1.getTo()), count + oldCount);
    }

    @Test
    public void remove() {
        commandQueue.removeAll();
        commandQueue.send(command1);
        commandQueue.send(command2);

        commandQueue.remove(command1.getTo());
        Assert.assertEquals(commandQueue.size(command1.getTo()), 0);

        commandQueue.remove(command2.getTo());
        Assert.assertEquals(commandQueue.size(command2.getTo()), 0);
    }

    @Test
    public void sendAndReceive() throws Exception {
        int oldCount1 = commandQueue.size(address1);
        int oldCount2 = commandQueue.size(address2);

        commandQueue.send(command1);
        commandQueue.send(command2);
        Assert.assertEquals(oldCount1 + 1, commandQueue.size(address1));
        Assert.assertEquals(oldCount2 + 1, commandQueue.size(address2));

        ICommand rCommand1 = commandQueue.receive(address1, false);
        Assert.assertEquals(oldCount1, commandQueue.size(address1));
        Assert.assertEquals(address2, rCommand1.getFrom());
        Assert.assertEquals(address1, rCommand1.getTo());

        ICommand rCommand2 = commandQueue.receive(address2, false);
        Assert.assertEquals(oldCount2, commandQueue.size(address2));
        Assert.assertEquals(address1, rCommand2.getFrom());
        Assert.assertEquals(address2, rCommand2.getTo());
    }
}
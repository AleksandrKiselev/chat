package service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import service.worker.IServiceWorker;

public class ServiceRegistryTest {

    private ServiceRegistry registry;
    private StubService stubService;

    private class StubService implements IService {
        private Address address = new Address();
        private ServiceType type = new ServiceType("stubService1");
        private boolean started = false;

        public boolean isStarted() {
            return started;
        }

        @Override
        public Address getAddress() {
            return address;
        }

        @Override
        public ServiceType getType() {
            return type;
        }

        @Override
        public boolean addWorker(IServiceWorker worker, boolean start) { return false; }

        @Override
        public boolean canAddWorker() { return false;}

        @Override
        public void start() {
            started = true;
        }

        @Override
        public void stop() {
            started = false;
        }
    }


    public ServiceRegistryTest() throws Exception  {
        registry = ServiceRegistry.instance;
        stubService = new StubService();
        if (registry.isRegistered(stubService.getType()))
            registry.unregister(stubService.getType());
        registry.register(stubService);
    }


    @Before
    public void setUp() throws Exception {
        registry.stopAll();
    }

    @After
    public void tearDown() throws Exception {
        registry.stopAll();
    }


    @Test
    public void instance() throws Exception {
        Assert.assertEquals(registry, ServiceRegistry.instance);
    }


    @Test
    public void register() throws Exception {
        /* 1. Регистрация сервиса-заглушки. */
        IService stubService = new IService() {
            private Address address = new Address();
            private ServiceType type = new ServiceType("stubService2");

            @Override
            public Address getAddress() { return address; }

            @Override
            public ServiceType getType() { return type; }

            @Override
            public boolean addWorker(IServiceWorker worker, boolean start) { return false; }

            @Override
            public boolean canAddWorker() { return false;}

            @Override
            public void start() { }

            @Override
            public void stop() { }
        };

        boolean notRegisteredException = false;
        try {
            registry.register(stubService);
        } catch (ServiceException e) {
            notRegisteredException = true;
        }
        Assert.assertFalse(notRegisteredException);

        /* 3. Повторная регистраиця.
              Ожидаемое поведение: ServiceException. */
        try {
            registry.register(stubService);
        } catch (ServiceException e) {
            notRegisteredException = true;
        }
        Assert.assertTrue(notRegisteredException);
    }


    @Test
    public void getService() throws Exception {
        /* 1. Получение незарегистрированного серивиса.
              Ожидаемое поведение: ServiceException. */
        boolean notRegisteredException = false;
        try {
            registry.getService(new ServiceType("temp"));
        } catch (ServiceException e) {
            notRegisteredException = true;
        }
        Assert.assertTrue(notRegisteredException);

        /* 2. Получение зарегистрированного сервиса. */
        notRegisteredException = false;
        IService service = null;
        try {
            service = registry.getService(stubService.getType());
        } catch (ServiceException e) {
            notRegisteredException = true;
        }

        Assert.assertFalse(notRegisteredException);
        Assert.assertEquals(stubService.getType(), service.getType());
    }


    @Test
    public void getAddress() throws Exception {
        /* 1. Получение адреса незарегистрированного серивиса.
              Ожидаемое поведение: ServiceException. */
        boolean notRegisteredException = false;
        try {
            registry.getAddress(new ServiceType("temp"));
        } catch (ServiceException e) {
            notRegisteredException = true;
        }
        Assert.assertTrue(notRegisteredException);

        /* 2. Получение адреса зарегистрированного сервиса. */
        notRegisteredException = false;
        Address address = null;
        try {
            address = registry.getAddress(stubService.getType());
        } catch (ServiceException e) {
            notRegisteredException = true;
        }

        Assert.assertFalse(notRegisteredException);
        Assert.assertEquals(stubService.getAddress(), address);
    }


    @Test
    public void startAll() throws Exception {
        registry.startAll();
        Assert.assertTrue(stubService.isStarted());
    }

    @Test
    public void interruptAll() throws Exception {
        registry.stopAll();
        Assert.assertFalse(stubService.isStarted());
    }

}
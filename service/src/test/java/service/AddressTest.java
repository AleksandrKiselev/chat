package service;

import org.junit.Assert;
import org.junit.Test;

public class AddressTest {

    @Test
    public void unique() {
        Address address1 = new Address();
        Address address2 = new Address();
        Address address3 = new Address();

        Assert.assertNotEquals(address1.hashCode(), address2.hashCode());
        Assert.assertNotEquals(address1.hashCode(), address3.hashCode());
        Assert.assertNotEquals(address2.hashCode(), address3.hashCode());
    }

}